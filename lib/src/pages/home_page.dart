import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:movies/src/providers/peliculas_provider.dart';
import 'package:movies/src/widgets/card_swipper_widget.dart';
import 'package:movies/src/widgets/movie_horizontal.dart';

class HomePage extends StatelessWidget {

  PeliculasProvider peliculasProvider = new PeliculasProvider();

  @override
  Widget build(BuildContext context) {

    peliculasProvider.getPopulares();

    return Scaffold(
      appBar: AppBar(
        title: Text("Películas en cines"),
        backgroundColor: Colors.indigoAccent,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: (){},
          )
        ],
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            _swipperTarjetas(),
            _footer(context)
          ],
        ),
      )

    );
  }

  Widget _swipperTarjetas() {
    return FutureBuilder(
      future: peliculasProvider.getEnCines(),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {

        if (snapshot.hasData) {
          return CardSwiper(
            peliculas: snapshot.data,
          );
        } else {
          return Container(
            height: 400.0,
              child: Center(
                  child: CircularProgressIndicator()
              )
          );
        }
      },
    );

    /*return CardSwiper(
        peliculas: [1, 2, 3, 4, 5]
    );*/
  }

  Widget _footer(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 20.0),
              child: Text("Populares", style: Theme.of(context).textTheme.headline6,)
          ),
          SizedBox(height: 5.0,),
          StreamBuilder(
            stream: peliculasProvider.popularesStream,
            builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
              if (snapshot.hasData) {
                 return MovieHorizontal(
                   peliculas: snapshot.data,
                   paginaSiguiente: peliculasProvider.getPopulares,
                 );
              } else {
                return Center(child: CircularProgressIndicator());
              }
            }
          ),
        ],
      ),
    );
  }

}
